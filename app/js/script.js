/* Author: Quinton Jason @quintonjasonjr */

// ==========================
/* Store current location */
// ==========================

var pathName = location.pathname;

// ==========================
/* Remove Phone Link on Desktop */
// ==========================

remPhoneLink = function(){
	docWidth = $(document).width();
	if (docWidth >= 1024){
		$('[data-query="phone"]').click(function(){
			return false;
		});
	}
	else{
		$('[data-query="phone"]').click(function(){
			return true;
		});
	}
};
remPhoneLink();

// ==========================
/* Adding and removing classes */
// ==========================

var addClassTo = function(query){
	$('[data-query="' + query + '"]').addClass('on');
};
var removeClassFrom = function(query){
	$('[data-query="' + query + '"]').removeClass('on');
};
var addClassByUrl = function(path,query){
	if(pathName.indexOf(path) >= 0){
		$('[data-query="' + query + '"]').addClass('on');
	}
};

// ==========================
/* Mobile Menu Trigger */
// ==========================

var nav = '';
var toggler = document.getElementById('mobile-toggle');

$(toggler).click(function(){
	$(nav).toggleClass('mobile-hidden').toggleClass('show');
});

// ==============================================
/* JVFloat - Placeholder Effect*/
// ==============================================

//apply effect
if(!$('html').hasClass('lt-ie9')){
	$('.float-pattern').jvFloat();
}

// ==============================================
/* Back to top */
// ==============================================

if($(window).width() > 767){
	
	var offset = 220;
	var duration = 500;

	var scrolltop = $(document).scrollTop();

	var bodyHeight = $('body').outerHeight();
	var bottomHeight = $('#bottom-footer').outerHeight();
	var pageHeight	= bodyHeight - bottomHeight;

	$(window).scroll(function() {
		if ($(this).scrollTop() > offset) {
			$('#back-to-top').fadeIn(duration);
		} else {
			$('#back-to-top').fadeOut(duration);
		}

		if  (scrolltop >= pageHeight) {
	        $('#back-to-top').addClass('abs-stmaadbox');
	    } else {
	        $('#back-to-top').addClass('fix-stmaadbox');
	    }
	});
	
	$('.back-to-top').click(function(e) {
		e.preventDefault();
		$('html, body').animate({scrollTop: 0}, duration);
		return false;
	})

}

// ==========================
/* MultiStep Form */
// ==========================

$('#contact_form').children('.form-section').first().children('.item-field').focus();

  //progress bar
  var totalSteps = 9;
	var barWidth = $('.barWrap').width();
	var prog = barWidth/totalSteps;
	var currentValue = 1;
  var maxValue = 9;

	// console.log(perc);

	$('#bar').css('width', prog);
//CODEPEN Form

//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches
// var tlNext = new TimelineLite();

$(".next").click(function(){
	console.log('clicked');
	if ($('#contact_form').parsley().validate({group: 'block-' + curIndex()})){
	    // if(animating) return false;
	    // animating = true;
	    
	    current_fs = $(this).parent();
	    next_fs = $(this).parent().next();
	    current_label = $(this).parent().children('label');

	    current_fs.children('label').css('transform', 'translateY(-100%)');
	    // next_fs.children('.item-field').get(0).focus();
	    setTimeout(function(){
	        next_fs.children('.item-field').focus();
	    }, 0);
	    console.log(next_fs.children('.item-field'));
	    
	    //activate next step on progressbar using the index of next_fs
	    $("#form-nav li").eq($("fieldset").index(next_fs)).addClass("active").removeClass("grayed");
	   //  currentValue++;
    // if (currentValue > maxValue)
    // 	currentValue = maxValue;
    
		// $('#bar').css('width', prog * currentValue);
		// $("#stepNum").text(currentValue);

		// $('html, body').animate({scrollTop: 515}, 500);
	
		$('html, body').animate({scrollTop: 0}, 500);

		var tlNext = new TimelineLite();
		tlNext
			.to(current_fs, .35, {autoAlpha: 0}, 'next-switch')
			
			.set(next_fs, { position : 'relative', className: 'current', xPercent:100})
			.set(current_fs, { position : 'absolute'})
			.to(next_fs, .5, {autoAlpha: 1, xPercent: 0, ease:Back.easeOut.config(.8)});
			

	    console.log(curIndex());
	    formResize();
	}
});

//binding the enter keypress
$('fieldset.current .item-field').keypress(function(e){
    if(e.which == 13){//Enter key pressed
    	// e.preventDefault();
        $('.next').click();//Trigger search button click event

    }
});

$(".previous").click(function(){
    // if(animating) return false;
    // animating = true;
    
    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    previous_fs.children('label').css('transform', 'translateY(0%)');
    $("#form-nav li").eq($("fieldset").index(current_fs)).removeClass("active").addClass('grayed');
    
    currentValue--;
    if (currentValue < 1)
    	currentValue = 1;
    
		$('#bar').css('width', prog * currentValue);
		$("#stepNum").text(currentValue);

		$('html, body').animate({scrollTop: 0}, 500);
    

    var tlNext = new TimelineLite();
	tlNext
		.to(current_fs, .5, {autoAlpha: 0}, 'next-switch')
		.set(previous_fs, { position : 'relative', className: 'current', xPercent:-100}, 'form-page-change')
		.set(current_fs, { position : 'absolute'}, 'form-page-change')
		.to(previous_fs, .5, {autoAlpha: 1, xPercent: 0, ease:Back.easeOut.config(.8)});

		

    formResize();
});

function formResize(){
	var setHeight = $('fieldset.current').height();
    $('.fields-wrapper').height(setHeight);
}

// var $sections = $('');
var $sections = $('.form-section');

function navigateTo(index) {
// Mark the current section with the class 'current'
$sections
  .removeClass('current')
  .eq(index)
    .addClass('current');

// Show only the navigation buttons that make sense for the current section:
$('.form-navigation .previous').toggle(index > 0);
var atTheEnd = index >= $sections.length - 1;
$('.form-navigation .next').toggle(!atTheEnd);
$('.form-navigation [type=submit]').toggle(atTheEnd);
}

function curIndex() {
// Return the current index by looking at which section has the class 'current'
return $sections.index($sections.filter('.current'));
}

// Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
$sections.each(function(index, section) {
$(section).find(':input').attr('data-parsley-group', 'block-' + index);
});
navigateTo(0); // Start at the beginning


// ==========================
/* Form Ajax */
// ==========================

// $("#contact_form").submit(function(e){
// 	e.preventDefault();
// 	// console.log('clicked');

// 	var $contactForm = $("#contact_form"),
// 		$thanksCopy = $(".thank-you-copy"),
// 		$ending = $('#ending'),
// 		$formEle = $(".anim"),
// 		$cloud = $("#cloud path"),
// 		$submitWrapper = $(".submit-wrapper"),
// 		$submitBox = $(".submit-wrapper svg"),
// 		$submitPath = $(".submit-wrapper path"),
// 		$morphDest = $("#cloud path"),
// 		tl;

// 	tl = new TimelineLite({paused:true});

// 	tl.set($contactForm, {overflow:"visible"})
// 	  .to($formEle, 1, {autoAlpha: 0})
// 	  .to($thanksCopy, 0.5, {autoAlpha:1, y:50, ease: Back.easeOut})
// 	  .to($submitBox, 0.5, {attr:{viewBox:"0 0 333.49 268.73"}}, "morphingTime")
// 	  .to($submitPath, 1, {morphSVG: $morphDest}, "morphingTime")
// 	  .to($submitBox, 10, {y:-275}, "rise")
// 	  .to($submitBox, 2, {rotation:"+=8", transformOrigin: "center top", ease:Power1.easeInOut, repeat:4, yoyo:true}, "rise")
// 	  .to($submitBox, 1, {x:"+=4", ease:Power1.easeInOut, repeat:8, yoyo:true}, "rise")
// 	  .to($submitBox, 2.5, {y:"+=10", ease:Power1.easeInOut, repeat: -1, yoyo:true});

// 	var $form = $(this),
// 		url = $form.attr('action');

// 		console.log(url);

// 	$.ajax({
// 		url: url,
// 		type: "post",
// 		dataType: "html",
// 		data: $form.serialize(),

// 		error: function(jqXHR, textStatus, errorThrown)
// 		{
// 			displayAjaxMessage("Sorry, there was an error submitting your form. Please try again. We apologize for this inconvenience");
// 		},

// 		success: function(data, textStatus, jqXHR){
// 			console.log('data', data);
// 			console.log('textStatus', textStatus);
// 			console.log('jqXhr', jqXHR);
// 			tl.play();
// 			console.log("ajax clicked");
// 		}
// 	});
// });

// ==========================
/* ScrollMagic */
// ==========================

if($('body').hasClass('scrollin')){
// init controller
	var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

	// // build scenes
	// new ScrollMagic.Scene({triggerElement: "#parallax1"})
	// 				.setTween("#parallax1 > div", {y: "80%", ease: Linear.easeNone})
	// 				// .addIndicators()
	// 				.addTo(controller);

	// new ScrollMagic.Scene({triggerElement: "#parallax2"})
	// 				.setTween("#parallax2 > div", {y: "80%", ease: Linear.easeNone})
	// 				// .addIndicators()
	// 				.addTo(controller);

	new ScrollMagic.Scene({triggerElement: "#parallax3"})
					.setTween("#parallax3 > div", {y: "80%", ease: Linear.easeNone})
					// .addIndicators()
					.addTo(controller);
}

// ==========================
/* Loading Screen */
// ==========================

$(document).ready(function(){
	// if($('html').hasClass('wf-loading')){
	// 	$(this).hide();
	// }	
	TweenMax.to('.loader9__inner', 2, {
	  rotation: -360,
	  repeat: -1,
	  transformOrigin: '50% 50%',
	  ease: Power0.easeNone
	});
	TweenMax.to('.loader9__outer', 3, {
	  rotation: 360,
	  repeat: -1,
	  transformOrigin: '50% 50%',
	  ease: Power0.easeNone
	});
});




// ==========================
/* Verify Page */
// ==========================

window.onbeforeunload = function(){
	window.scrollTo(0,0);
}

if($('body').hasClass('home')){
	TweenMax.set("#verify-wrapper .verified", {autoAlpha: 0})

	var verifyHeight = $(window).outerHeight();
	// $('#verify-wrapper').height(verifyHeight);

	$('#verify-button').click(function(){
		TweenMax.to("#verify-wrapper .verify-top", .5, {autoAlpha: 0});
		TweenMax.to('#verify-wrapper .verified', .5, {autoAlpha: 1})
		// TweenMax.to("#ccp-logo", 2, { morphSVG: "#ss-logo" })
		// moveClouds();
		TweenMax.to('#verify-wrapper .verified', .5, {autoAlpha: 1})
		TweenMax.to("#Balloons_Image", 2, { autoAlpha: 1 })
		$('#verify-wrapper .verified-mark').addClass('on');
		if($(window).width() > 768){
			$('body').addClass('nav-on');
		}	
		$('body').removeClass('overflow-on');
	});
	if($(window).width() > 768){
		$('body').addClass('nav-on');
	}	
}

// ==========================
/* Fixed Nav */
// ==========================

if($(window).width() > 768 && $('body').hasClass('home')){
 $(window).scroll(function(){
        var window_top = $(window).scrollTop() + 12; // the "12" should equal the margin-top value for nav.stick
        var div_top = $('nav').offset().top;
            if (window_top > div_top) {
                $('nav').addClass('stick');
            } else {
                $('nav').removeClass('stick');
            }
    });

    /**
     * This part causes smooth scrolling using scrollto.js
     * We target all a tags inside the nav, and apply the scrollto.js to it.
     */
    $("nav a").click(function(evn){
        evn.preventDefault();
        $('html,body').scrollTo(this.hash, this.hash); 
    });

    /**
     * This part handles the highlighting functionality.
     * We use the scroll functionality again, some array creation and 
     * manipulation, class adding and class removing, and conditional testing
     */
    var aChildren = $("nav li").children(); // find the a children of the list items
    var aArray = []; // create the empty aArray
    for (var i=0; i < aChildren.length; i++) {    
        var aChild = aChildren[i];
        var ahref = $(aChild).attr('href');
        aArray.push(ahref);
    } // this for loop fills the aArray with attribute href values

    $(window).scroll(function(){
        var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
        var windowHeight = $(window).height(); // get the height of the window
        var docHeight = $(document).height();

        for (var i=0; i < aArray.length; i++) {
            var theID = aArray[i];
            var divPos = $(theID).offset().top; // get the offset of the div from the top of page
            var divHeight = $(theID).height(); // get the height of the div in question
            if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
                $("a[href='" + theID + "']").addClass("nav-active");
            } else {
                $("a[href='" + theID + "']").removeClass("nav-active");
            }
        }

        if(windowPos + windowHeight == docHeight) {
            if (!$("nav li:last-child a").hasClass("nav-active")) {
                var navActiveCurrent = $(".nav-active").attr("href");
                $("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
                $("nav li:last-child a").addClass("nav-active");
            }
        }
    });
}


// ==========================
/* MORPH trigger - object tag */
// ==========================

if($('body').hasClass('home')){
	var svgTrigger = document.getElementById("verify-button");
	var mySVG = document.getElementById("ccp-ss");
	var myCloud = document.querySelector(".top-cloud");

	mySVG.addEventListener("load", function() {

	   var doc = this.getSVGDocument();

	   //SVG vars
	   var ccpLogo = doc.getElementById("ccp-logo"); 
	   var ssLogo = doc.getElementById("ss-logo"); 
	   var cloud = doc.getElementById("cloud1"); 
	   var tl = new TimelineLite({paused:true});

	   tl
	   	.to(ccpLogo, 2, { morphSVG: ssLogo },'morph1')
	   	// .to(ccpLogo, 2, { morphSVG: cloud }, 'morph1+=13')
	   	// .to(mySVG, 50, {x:'-500%', scale: 1.5, ease: Power0.easeNone}, 'morph1+=13');

	   //CSS Init styles
	   ssLogo.style.opacity = 0;
	   cloud.style.opacity = 0;

	   //happy tweening!
	   svgTrigger.addEventListener("click", function(){
	   	tl.play();
	   });
	  
	});	

	//bug fix regarding load event not firing consistantly 
	//(http://stackoverflow.com/questions/34677628/load-event-not-fired-on-safari-when-reloading-page)
	mySVG.data = mySVG.data;
}

function moveClouds(){
	var w = $(window).width();
	var ease = Power0.easeNone;
	TweenMax.fromTo('.cloud1', 30, {x:w},{x:'-100%', ease: ease, repeat: -1});
	TweenMax.fromTo('.cloud2', 42, {x:w/2},{x:'-100%', ease: ease, repeat: -1});
	TweenMax.fromTo('.cloud3', 56, {x:w/3},{x:'-100%', ease: ease, repeat: -1});
	TweenMax.fromTo('.cloud4', 66.5, {x:w},{x:'-100%', ease: ease, repeat: -1});
}

// ==========================
/* Form Input File Type */
// ==========================

var inputs = document.querySelectorAll( '.inputfile' );
Array.prototype.forEach.call( inputs, function( input )
{
	var label	 = input.nextElementSibling,
		labelVal = label.innerHTML;

	input.addEventListener( 'change', function( e )
	{
		console.log('changed');
		var fileName = '';
		if( this.files && this.files.length > 1 )
			fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
		else
			fileName = e.target.value.split( '\\' ).pop();

		if( fileName )
			label.querySelector( 'span' ).innerHTML = fileName;
		else
			label.innerHTML = labelVal;
	});

	// Firefox bug fix
	input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
	input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
});



//testing and trying to make judges tween modular
(function(){
	"use strict";
}());

// ==========================
/* Judges Tween */
// ==========================
(function(){
	"use strict";
	if($("body").hasClass("home")){
		//random tween
		var judgePos = document.querySelector("#scot-bio").getBoundingClientRect();
		var judgeTop = judgePos.top;
		var judgeLeft = judgePos.left;
		var judgeWidth = judgePos.width;
		var judgeHeight = judgePos.height;

		var scotTrigger = $("#scot-trigger"),
			scotBio = $("#scot-bio"),
			judgeWrapper = $(".judge-wrapper"),
			closeBio = $("#scot-bio .close-bio"),
			brownBG = $("#judges div.brown-bg"),
			tl;

		tl = new TimelineLite({paused:true});

		TweenLite.set(scotBio, {autoAlpha:0});

		tl.set(brownBG, {className:"+=on"})
		  .to(judgeWrapper, 0.5, {opacity: 0}, 0)
		  .to("#judges", 0.5, {backgroundColor: "rgba(0,0,0,0.75)"}, 0)
		  .set(brownBG,{clearProps: "all"})
		  .to("#scot-bio .units-row", .5, { opacity: 1 }, "switch")
		  .to(brownBG, 0.5, { width: judgeWidth, height: judgeHeight }, "switch")
		  // .to(scotBio, 1, { autoAlpha:1, scale:1, top: "50%",left:"50%", width: 'auto', height: 'auto', ease: Back.easeOut }, "switch")
		  .to(scotBio, 0.25, { autoAlpha:1, scale:1 })

		scotTrigger.click(function(e) {
			e.preventDefault();
			tl.play();
		});

		closeBio.click(function(e) {
			e.preventDefault();
			tl.reverse();
		});

		//random tween
		var leftjudgePos = document.querySelector("#dan-bio").getBoundingClientRect();
		var leftjudgeTop = leftjudgePos.top;
		var leftjudgeLeft = leftjudgePos.left;
		var leftjudgeWidth = leftjudgePos.width;
		var leftjudgeHeight = leftjudgePos.height;

		var danTrigger = $("#dan-trigger"),
			danBio = $("#dan-bio"),
			leftjudgeWrapper = $(".judge-wrapper"),
			leftcloseBio = $("#dan-bio .close-bio"),
			leftbrownBG = $("#judges div.brown-bg"),
			lefttl;

		lefttl = new TimelineLite({paused:true});

		TweenLite.set(danBio, {autoAlpha:0});

		lefttl
		  .set(leftbrownBG, {className:"+=on"})
		  .to(leftjudgeWrapper, 0.5, {opacity: 0})
		  .to("#judges", 0.5, {backgroundColor: "rgba(0,0,0,0.75)"})
		  .set(leftbrownBG,{clearProps: "all"})
		  .to("#dan-bio .units-row", .5, { opacity: 1 }, "switch")
		  .to(leftbrownBG, 0.5, { width: leftjudgeWidth, height: leftjudgeHeight }, "switch")
		  .to(danBio, 0.25, { autoAlpha:1, scale:1 })

		danTrigger.click(function(e) {
			e.preventDefault();
			lefttl.play();

		});

		leftcloseBio.click(function(e) {
			e.preventDefault();
			lefttl.reverse();
		});

	}
}());
