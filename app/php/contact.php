<?php
require 'vendor/autoload.php';

$lang = 'en';


// Dotenv::load(__DIR__);

$sendgrid_username = 'xdesigninc';
$sendgrid_password = '5C071Z483201o407';


if (isset($_POST['submit']) && $_FILES["salad-pic"]) {
      $from = $_POST['email'];

      //form vars
      $personsName = $_POST['name'];
      $personsBusiness = $_POST['business'];
      // $personsAddress = $_POST['address'];
      $personsCity = $_POST['city'];
      // $personsState = $_POST['state'];
      // $personsZip = $_POST['zip'];
      // $personsPhone = $_POST['phone'];
      $personsSalesperson = $_POST['salesperson'];

      $personsDish = $_POST['dish'];
      $personsIngredients = $_POST['ingredients'];
      // $personsCCPItems = $_POST['ccp-items'];
      $personsDirections = $_POST['directions'];
      // $personsGarnish = $_POST['garnish'];
      // $personsIce = $_POST['ice'];
      // $personsGlass = $_POST['glass'];
      // $personsStory = $_POST['story'];
      // $personsName = $_POST['name'];
      // $personsPhone = $_POST['phone'];
      // $personsHistory = $_POST['history'];

      $filePath = dirname(__FILE__) .'/../salad/uploads';
      $uploaddir = dirname(__DIR__) .'local.ccpsaladcontest.com/salad/uploads/';

      $namePic = '';
      $mimePic = '';
      $tempPic = '';
      // if (isset($_FILES["salad-pic"]["name"])) {
      $uploadfile = $uploaddir . basename($_FILES['salad-pic']['name']);
      $finalPic = base64_encode(file_get_contents($_FILES['salad-pic']['tmp_name']));
      $namePic = $_FILES['salad-pic']['name'];
      $tempPic = $_FILES['salad-pic']['tmp_name'];
      $mimePic = $_FILES['salad-pic']['type'];


      $emailSubject = 'Salad Submission from ' . $personsName;

      // $messagePlain = ;

      $message = '<p><strong>Chef\'sName: </strong>' . $personsName . '<p>
            <p><strong>Email: </strong>' . $from . '<p>
            <p><strong>Business Name: </strong>' . $personsBusiness . '</p> 
            <p><strong>City: </strong>' . $personsCity . '</p> 
            <p><strong>Salesperson Name: </strong>' . $personsSalesperson . '</p> 

            <p><strong>Name of Dish: </strong>' . $personsDish . '</p> 
            <p><strong>Ingredients (Including Dressing): </strong>' . $personsIngredients . '</p> 
            <p><strong>Directions (Including Dressing): </strong>' . $personsDirections . '</p> 
            
              ';

      $sendgrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));


      $email = new SendGrid\Email();
      // $emails = array('britteny@thinkx.net','quinton@thinkx.net');
      $emails = array('ahudnall@capitolcityproduce.com','hunter@thinkx.net','paige@thinkx.net','quinton@thinkx.net');
      // $emails = array('quinton@thinkx.net');

      $email->setTos($emails)->
             setFrom($from)->
             setSubject($emailSubject)->
             setText(' ')->
             setAttachment($tempPic, $namePic)->
             setHtml($message)->
             addHeader('X-Sent-Using', 'SendGrid-API')->
             addHeader('X-Transport', 'web');


      // var_dump($email); die();

      $response = $sendgrid->send($email);
}

// $recaptcha = new \ReCaptcha\ReCaptcha($secret);
// $resp = $recaptcha->verify($gRecaptchaResponse, $remoteIp);
// if ($resp->isSuccess()) {
//     // verified!
//   $response = $sendgrid->send($email);
// } else {
//     $errors = $resp->getErrorCodes();
// }

// $response = $sendgrid->send($email);

// print_r($response);
header('Location: /thank-you.html'); 
exit();